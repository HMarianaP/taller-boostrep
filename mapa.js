function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.602377658575274,-74.06306773424149], zoom:18 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorBOGOTA= L.marker([4.602377658575274,-74.06306773424149]);
    marcadorBOGOTA.addTo(mapa);

    var polygon = L.polygon([
        [4.602377658575274,-74.06306773424149],
        [4.602377658575274, -74.06252056360245],
        [ 4.60276799828148, -74.06252056360245],
        [4.60276799828148, -74.06306773424149],
        [4.602377658575274, -74.06306773424149]
    ]).addTo(mapa);
 
}